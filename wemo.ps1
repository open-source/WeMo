$wsdl = 'https://gitlab.tpic.com.au/open-source/WeMo/raw/master/BasicService.wsdl'
$webservice = New-WebServiceProxy -Uri $wsdl -Namespace WebServiceProxy -Class WemoControl

$type = $webservice.GetType().NameSpace

Function Set-WemoEndpoint([string] $ip){
    #[string] $ip = "192.168.0.193"; #145
    [int32] $port = 49153;
    [string] $url = $("http://" + $ip + ":" + $port + "/upnp/control/basicevent1")
    $webservice.url = $url
}

Function Get-WeMoState(){
    $GetStateRequest = New-Object($type + ".GetBinaryState")
    $state = $webservice.GetBinaryState($GetStateRequest)
    return $state.BinaryState
}

Function Set-WeMoState([bool] $state){
    $SetStateRequest = New-Object($type + ".SetBinaryState")
    $SetStateRequest.BinaryState = [int32] $state
    $status = $webservice.SetBinaryState($SetStateRequest)
    return $status.BinaryState
}

Function Get-WeMoName(){
    $GetNameRequest = New-Object($type + ".GetFriendlyName")
    $name = $webservice.GetFriendlyName($GetNameRequest)
    return $name.FriendlyName
}

Function Set-WeMoName([string] $name){
    $ChangeNameRequest = New-Object($type + ".ChangeFriendlyName")
    $ChangeNameRequest.FriendlyName = $name
    $name = $webservice.ChangeFriendlyName($ChangeNameRequest)
    return $name.FriendlyName
}

Function Get-WeMoSignalStrength(){
    $GetSignalStrengthRequest = New-Object($type + ".GetSignalStrength")
    $signal = $webservice.GetSignalStrength($GetSignalStrengthRequest)
    return $signal.SignalStrength
}

Function Get-WeMoHashCode(){
    return $webservice.GetHashCode()
}

Function Get-WeMoHomeId(){
    $GetHomeIdRequest = New-Object($type + ".GetHomeId")
    $homeid = $webservice.GetHomeId($GetHomeIdRequest)
    return $homeid.HomeId
}

Function Get-WeMoIconUrl(){
    $GetIconURLRequest = New-Object($type + ".GetIconURL")
    $GetIconURL = $webservice.GetIconURL($GetIconURLRequest)
    return $GetIconURL.URL
}

Function Get-WeMoLifetimeService(){
    return $webservice.GetLifetimeService()
}

Function Get-WeMoLogFileUrl(){
    $GetLogFileURLRequest = New-Object($type + ".GetLogFileURL")
    $GetLogFileURL = $webservice.GetLogFileURL($GetLogFileURLRequest)
    return $GetLogFileURL.LOGURL
}

Function Get-WemoInfo([Object[]] $ips){
    $output = [Object[]] @()
    
    $ips | ForEach-Object{

        $ip = $_
        Set-WemoEndpoint $ip
        $info = [pscustomobject] @{
            Name = Get-WemoName
            Address = $ip
            State = Get-WeMoState
            SignalStrength = Get-WeMoSignalStrength
            LifeTimeService = Get-WeMoLifetimeService
            IconUrl = Get-WeMoIconUrl
            LogFileUrl = Get-WeMoLogFileUrl
            HashCode = Get-WeMoHashCode
        }
        $output += $info

    }

    $output

}


Get-WemoInfo -ips @("192.168.0.145","192.168.0.193")