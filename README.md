# WeMo control via PowerShell WebServiceProxy
This script accesses the Belkin WeMo SOAP interface to control WeMo devices.

# Sources
- https://github.com/sklose/WeMoWsdl